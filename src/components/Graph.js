import './Graph.css';
import datasetJson from '../dataset.json';
import { Dropdown, DropdownButton } from 'react-bootstrap';
import { useState } from "react";
import {BarChart, CartesianGrid, Legend, ResponsiveContainer, XAxis, YAxis, Tooltip, Bar} from "recharts";

const Graph = () => {
    const datasetKeys = Object.keys(datasetJson);
    const [selectedInputData, setSelectedInputData] = useState([]);
    const [selectedOutputData, setSelectedOutputData] = useState([]);
    const [ovenTempData, setOvenTempData] = useState([]);
    const [viscosityData, setViscosityData] = useState([]);

    const setData = (dataKey) => {
        const data = datasetJson[dataKey];
        const inputData = [];
        const outputData = [];
        const inputKeys = Object.keys(data["inputs"]);
        const outputKeys = Object.keys(data["outputs"]);

        inputKeys.forEach((value) => {
            if (value === "Oven Temperature") {
                setOvenTempData([{
                    "name": value,
                    "oven temp": data["inputs"][value]
                }])
            } else {
                inputData.push({
                    "name": value,
                    "inputs": data["inputs"][value]
                });
            }
        });

        outputKeys.forEach((value) => {
            if (value === "Viscosity") {
                setViscosityData([{
                    "name": value,
                    "viscosity": data["outputs"][value]
                }])
            } else {
                outputData.push({
                    "name": value,
                    "outputs": data["outputs"][value]
                });
            }
        })

        console.log(inputData);

        setSelectedInputData(inputData);
        setSelectedOutputData(outputData);

        console.log(`${selectedInputData}`);
    }

    return (
        <div className="graph-container">
            <div className="dropdown-container">
                <DropdownButton id="dataset-button" title="Datasets">
                        {datasetKeys.map((key, index) => {
                            return <Dropdown.Item
                                as="button"
                                key={key}
                                onClick={ () => { setData(key) } }
                            >
                                {key}
                            </Dropdown.Item>
                        })}
                </DropdownButton>
            </div>
            <div className="responsive-container-container">
                    <ResponsiveContainer width="100%" height="100%">
                        <BarChart
                            width={500}
                            height={300}
                            data={selectedInputData}
                            margin={{
                                top: 5,
                                right: 30,
                                left: 20,
                                bottom: 5
                            }}
                        >
                            <CartesianGrid strokeDasharray="3 3"/>
                            <XAxis dataKey="name"/>
                            <YAxis/>
                            <Tooltip/>
                            <Legend />
                            <Bar dataKey="inputs" fill="#c88feb"/>
                        </BarChart>
                    </ResponsiveContainer>
                    <ResponsiveContainer width="10%" height="100%">
                        <BarChart
                            width={500}
                            height={500}
                            data={ovenTempData}
                            margin={{
                                top: 5,
                                right: 30,
                                left: 20,
                                bottom: 5
                            }}
                        >
                            <CartesianGrid strokeDasharray="3 3"/>
                            <XAxis dataKey="name"/>
                            <YAxis/>
                            <Tooltip/>
                            <Legend/>
                            <Bar dataKey="oven temp" fill="#fa7b5f"/>
                        </BarChart>
                    </ResponsiveContainer>
                <div className="outputs-container">
                    <ResponsiveContainer width="25%" height="100%">
                        <BarChart
                            width={500}
                            height={500}
                            data={selectedOutputData}
                            margin={{
                                top: 5,
                                right: 30,
                                left: 20,
                                bottom: 5
                            }}
                        >
                            <CartesianGrid strokeDasharray="3 3"/>
                            <XAxis dataKey="name"/>
                            <YAxis/>
                            <Tooltip/>
                            <Legend/>
                            <Bar dataKey="outputs" fill="#65a386"/>
                        </BarChart>
                    </ResponsiveContainer>
                    <ResponsiveContainer width="10%" height="100%">
                        <BarChart
                            width={500}
                            height={500}
                            data={viscosityData}
                            margin={{
                                top: 5,
                                right: 30,
                                left: 20,
                                bottom: 5
                            }}
                        >
                            <CartesianGrid strokeDasharray="3 3"/>
                            <XAxis dataKey="name"/>
                            <YAxis/>
                            <Tooltip/>
                            <Legend/>
                            <Bar dataKey="viscosity" fill="#7787c9"/>
                        </BarChart>
                    </ResponsiveContainer>
                </div>
            </div>
        </div>
    );
}

export default Graph;